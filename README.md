# DQRemakeSDK
The Software Development Kit version of the Dragon Quest Remake Engine, for forking and bugtesting

Included in this repo is an unencrypted version of the same customized scripts used to make up the engine used for Dragon Quest 1 Remake and all subsequent games using the same engine. It is released to the public for forking for use in their own attempts to make a game in RPG Maker VX Ace, for helping me with debugging scripting bugs, and because I like open sourcing my work where possible.

A few commercial scripts were removed for legal reasons like the Effectus script and replaced with free alternatives where possible. 

In it's entirety, this SDK is to be used non commercially, if used in it's entirety. Individual scripts and graphics associated with them have their own conditions (all compatible with the above to my knowledge), and they must be used according to the whims of the authors of each.

This can be opened and edited with the RPG Maker VX Ace Editor, and the data files can also be read with the Gemini RGSS Script Editor.

In it's present state, it WILL require tweaking for use in creating a another game if one decides to fork it, so basic knowledge of Ruby script editing is recommended.

If anyone has further questions, please contact me at;

arcane@live.com